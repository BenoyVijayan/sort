//
//  main.swift
//  Sort
//
//  Created by Binoy Vijayan on 3/31/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

let sort = Sort()

var arr:Array<Int> = [34 , 64 , 25, 12, 22, 11, 80, 74 , 100]
sort.bubbleSort( &arr)
print("Bubble sorted array :\(arr)")

arr = [34 , 64 , 25, 12, 22, 11, 80, 74 , 100]
sort.selectionSort(&arr)
print("Selection sorted array :\(arr)")

arr = [34 , 64 , 25, 12, 22, 11, 80, 74 , 100]
sort.insertionSort(&arr)
print("Inserion sorted array :\(arr)")

arr = [34 , 64 , 25, 12, 22, 11, 80, 74 , 100]
sort.mergeSort(&arr)
print("Merge sorted array :\(arr)")

arr = [34 , 64 , 25, 12, 22, 11, 80, 74 , 100]
sort.quickSort(&arr)
print("Quick sorted array :\(arr)")

