////////////////////////////////////////////////////////
//
//  This software is released as per the clauses of MIT License
//
//
//  The MIT License
//
//  Copyright (c) 2016, Binoy Vijayan.
//
//                      benoy.apple@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
//

import Foundation

class Sort{
    
/*******************************************************************************************************************
    BUBBLE SORT
******************************************************************************************************************//*
    Bubble sort is based on the idea of repeatedly comparing pairs of adjacent elements and then
    swapping their positions if they exist in the wrong order.
     
    Bubble sort has a worst-case and average complexity of О(n^2), where n is the number of items being sorted.
     
     Best case          : Ω(n)
     Average case       : Θ(n^2)
     Worst case         : O(n^2)
*/
    
    public func bubbleSort(_ array: inout Array<Int>){
        var swaped:Bool = false
        for idx in 0..<array.count-1{
            for jdx in 0..<array.count - (idx + 1){
                if array[jdx] > array[jdx + 1] {
                    let temp = array[jdx]
                    array[jdx] = array[jdx + 1]
                    array[jdx + 1] = temp
                    swaped = true
                }
            }
            if swaped == false { break }
        }
    }
    
/*******************************************************************************************************************/
 
    
/*******************************************************************************************************************
     SELECTION SORT
 ***************************************************************************************************************//*
 
     The selection sort algorithm sorts an array by repeatedly finding the minimum element
     (considering ascending order) from unsorted part and putting it at the beginning.
     The algorithm maintains two subarrays in a given array.
     
     1) The subarray which is already sorted.
     2) Remaining subarray which is unsorted.
     
     In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.
     
     Time Complexity: O(n2) as there are two nested loops.
     
     Auxiliary Space: O(1)
     
     The good thing about selection sort is it never makes more than O(n) swaps and can be useful
     when memory write is a costly operation.
     
     Best case          : Ω(n^2)
     Average case       : Θ(n^2)
     Worst case         : O(n^2)
 */
 
    public func selectionSort(_ array: inout Array<Int>){
        
        for idx in 0...array.count - 1{
            let  least = findMinimumIn(array,idx)
            let first:Int = array[idx]
            array[idx] = least.0
            array[least.1] = first
        }
    }
    
    private func findMinimumIn( _ array:Array<Int>, _ from:Int)-> (Int,Int){
        var result:Int = array[from]
        var index:Int = from
        for idx in (from + 1)..<array.count{
            if result > array[idx] {
                result = array[idx]
                index = idx
            }
        }
        
        return (result,index)
    }
/*******************************************************************************************************************/
    
    
/*******************************************************************************************************************
     INSERTION SORT
 ***************************************************************************************************************//*
    
     Insertion sort is a simple sorting algorithm that builds the final sorted array (or list) one item at a time.
     It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.
     
     Auxiliary Space        :   O(1)
     Boundary Cases         :   Insertion sort takes maximum time to sort if elements are sorted in reverse order.
                                And it takes minimum time (Order of n) when elements are already sorted.
     Algorithmic Paradigm   :   Incremental Approach
     Sorting In Place       :   Yes
     Stable                 :   Yes
     Online                 :   Yes
     
     Best case          : Ω(n)
     Average case       : Θ(n^2)
     Worst case         : O(n^2)
*/
    
    public func insertionSort(_ array: inout Array<Int>){
        for idx in 0..<array.count{
            let element:Int = array[idx]
            if idx == 0 { continue }
            for jdx in (0..<idx).reversed(){
                if element < array[jdx]{
                    let temp = array[jdx]
                    array[jdx] = element
                    array[jdx + 1] = temp
                }
            }
        }
    }
    
/*******************************************************************************************************************/
  
/*******************************************************************************************************************
 MERGE SORT
 ***************************************************************************************************************//*
    Like QuickSort, Merge Sort is a Divide and Conquer algorithm. It divides input array in two halves, calls
    itself for the two halves and then merges the two sorted halves. The merge() function is used for merging
    two halves. The merge(arr, l, m, r) is key process that assumes that arr[l..m] and arr[m+1..r] are sorted
    and merges the two sorted sub-arrays into one.
    
    Time complexity of Merge Sort is    :   \Theta(nLogn) in all 3 cases (worst, average and best) as merge
                                            sort always divides the array in two halves and take linear time to
                                            merge two halves.
    Auxiliary Space                     :   O(n)
    Algorithmic Paradigm                :   Divide and Conquer
    Sorting In Place                    :   No in a typical implementation
    Stable                              :   Yes
     
    Best case          : Ω(n log(n))
    Average case       : Θ(n log(n))
    Worst case         : O(n log(n))
*/

    public func mergeSort(_ array: inout Array<Int> ){
        mergeSort(&array, 0, array.count - 1)
    }
    
    private func mergeSort(_ array: inout Array<Int> , _ left:Int , _ right:Int ){
        
        if left < right{
            let middle:Int = (left + right) / 2
            __mergeSort(&array , left , middle)
            __mergeSort(&array , middle + 1 , right)
            merge( &array,left,middle,right)
        }
    }
    
    private func merge(_ array: inout Array<Int>, _ left:Int, _ middle:Int , _ right:Int ){
        
        let count1:Int = (middle - left) + 1
        let count2:Int = right - middle
        var leftArray:Array<Int> = Array<Int>()
        var rightArray:Array<Int> = Array<Int>()
        for i in 0..<count1{
            leftArray.append(array[ left + i ])
        }
        
        for j in 0..<count2{
            rightArray.append(array[ middle + 1 + j])
        }
        var idx:Int = 0
        var jdx:Int = 0
        var kdx:Int = left
        while idx < count1 && jdx < count2{
            if leftArray[idx] <= rightArray[jdx]{
                array[kdx] = leftArray[idx]
                idx += 1
            }else{
                array[kdx] = rightArray[jdx]
                jdx += 1
            }
            kdx += 1
        }
        
        while idx < count1{
            array[kdx] = leftArray[idx]
            idx += 1
            kdx += 1
        }
        
        while jdx < count2{
            array[kdx] = rightArray[jdx]
            jdx += 1
            kdx += 1
        }
    }
    
/*******************************************************************************************************************/

/*******************************************************************************************************************
 QUICK SORT
 ***************************************************************************************************************//*

     QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given array around the picked pivot. There are many different versions of quickSort that pick pivot in different ways.
 
     Always pick first element as pivot.
     Always pick last element as pivot (implemented below)
     Pick a random element as pivot.
     Pick median as pivot.
     
     Best case          : Ω(n log(n))
     Average case       : Θ(n log(n))
     Worst case         : O(n^2)
*/

    public func quickSort(_ array: inout Array<Int> ){
        let low = 0
        let high = array.count - 1
        quickSort(&array, low, high)
    }
    
    private func quickSort(_ array: inout Array<Int>, _ low:Int , _ high:Int ){
        if low < high {
            let pi = partition(&array, low, high);
            quickSort(&array, low, pi - 1);
            quickSort(&array, pi + 1, high);
        }
    }
    
    private func partition(_ array: inout Array<Int>, _ low:Int, _ high:Int)->Int{
        
        let pivot:Int = array[high]
        var idx:Int = low - 1
        for jdx in low..<high{
            if array[jdx] <= pivot {
                idx += 1
                let temp = array[idx]
                array[idx] = array[jdx]
                array[jdx] = temp
            }
        }
        let temp = array[idx + 1];
        array[idx + 1] = array[high]
        array[high] = temp
        
        return idx + 1
    }

}

