#Sorting Algorithms in Swift 4

####BUBBLE SORT
 Bubble sort is based on the idea of repeatedly comparing pairs of adjacent elements and then
    swapping their positions if they exist in the wrong order.
     
Bubble sort has a worst-case and average complexity of О(n^2), where n is the number of items being sorted.
     
**Best case          : Ω(n)**

**Average case       : Θ(n^2)**

**Worst case         : O(n^2)**

----
----

####SELECTION SORT

The selection sort algorithm sorts an array by repeatedly finding the minimum element
     (considering ascending order) from unsorted part and putting it at the beginning.
     The algorithm maintains two subarrays in a given array.
     
1) The subarray which is already sorted.

2) Remaining subarray which is unsorted.
     
In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.
     
**Time Complexity: O(n2)** as there are two nested loops.
     
**Auxiliary Space: O(1)**
     
The good thing about selection sort is it never makes more than O(n) swaps and can be useful when memory write is a costly operation.
     
**Best case          : Ω(n^2)**

**Average case       : Θ(n^2)**

**Worst case         : O(n^2)**

----
----

####INSERTION SORT

Insertion sort is a simple sorting algorithm that builds the final sorted array (or list) one item at a time.

It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.
     
**Auxiliary Space        :   O(1)**

**Boundary Cases**       :   Insertion sort takes maximum time to sort if elements are sorted in reverse order. And it takes minimum time (Order of n) when elements are already sorted.

**Algorithmic Paradigm   :   Incremental Approach**

**Sorting In Place       :   Yes**

**Stable                 :   Yes**

**Online                 :   Yes**
     
**Best case          : Ω(n)**

**Average case       : Θ(n^2)**

**Worst case         : O(n^2)**

----
----

####MERGE SORT

Like QuickSort, Merge Sort is a Divide and Conquer algorithm. It divides input array in two halves, calls
    itself for the two halves and then merges the two sorted halves. The merge() function is used for merging
    two halves. The merge(arr, l, m, r) is key process that assumes that arr[l..m] and arr[m+1..r] are sorted
    and merges the two sorted sub-arrays into one.
    
  Time complexity of Merge Sort is    :   Average case : Θ(nLogn) in all 3 cases (worst, average and best) as merge sort always divides the array in two halves and take linear time to merge two halves.
    
**Auxiliary Space   :   O(n)**

**Algorithmic Paradigm      :   Divide and Conquer**

**Sorting In Place          :   No in a typical **

**implementation**

**Stable             :   Yes**
     
**Best case          : Ω(n log(n))**

**Average case       : Θ(n log(n))**

**Worst case         : O(n log(n))**

----
----

####QUICK SORT

 QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions the given array around the picked pivot. There are many different versions of quickSort that pick pivot in different ways.
 
* Always pick first element as pivot.

* Always pick last element as pivot (implemented below)
Pick a random element as pivot.

* Pick median as pivot.
     
**Best case          : Ω(n log(n))**

**Average case       : Θ(n log(n))**

**Worst case         : O(n^2)**